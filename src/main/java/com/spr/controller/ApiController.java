package com.spr.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spr.model.Employee;
import com.spr.service.EmployeeService;


@Controller
@RequestMapping(value="/api")
public class ApiController {
	
	@Autowired	
	private EmployeeService empService;
	
	final static	public	int	DefaultPageSize = 5;
	
	@RequestMapping(value="/employee/all", method=RequestMethod.GET)
	@ResponseBody
	public List<Employee> allEmployees () {
		return  empService.allEmployees();		
	}
	
	@RequestMapping(value="/employee/page/{pg_index}", method=RequestMethod.GET)
	@ResponseBody
//	@SuppressWarnings("unchecked")
	public Map<String, Object>	listEmployeesPageable (@PathVariable int pg_index) {
		return  empService.listEmployeesPageable( pg_index, DefaultPageSize);		
	}
	
	@RequestMapping(value="/employee/age/{age}", method=RequestMethod.GET)
	@ResponseBody
	public List<Employee> listEmployeesSameAge ( @PathVariable int age) {
		System.out.println("=============== listemployeeOverAge>" + age );
		return  empService.listEmployeesSameAge(age);		
	}
	
	
	@RequestMapping(value="/employee/over/{age}", method=RequestMethod.GET)
	@ResponseBody
	public List<Employee> listEmployeesOverAge ( @PathVariable int age) {
		System.out.println("=============== listemployeeOverAge>" + age );
		return  empService.listEmployeesOverAge(age);		
	}
	
	@RequestMapping(value="/employee/search", method=RequestMethod.GET)
	@ResponseBody
	public List<Employee> searchEmployeeWithCriteria ( 
			@RequestParam("likename") 	String like_name, 
			@RequestParam("age") 	int age) {
		System.out.println( String.format("searchEmployeeWithCriteria (%s,%d)\n", like_name, age) );
		return  empService.listEmployeesWithCriteria(like_name, age);		
	}
	

}

